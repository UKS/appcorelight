import Foundation
import os.log
import AsyncNinja
import Essentials

public extension AppCore {
    static func log(title: String, msg: String, thread: Bool = false) {
        guard shouldPass(title: title) else { return }
        
        if thread {
            #if DEBUG
            let thread = Thread.current.dbgName.replace(of: "/Users/loki/dev/", to: "")
                .replace(of: "NSOperationQueue Main Queue", to: "Q MAIN")
            #else
            let thread = Thread.current.dbgName
            #endif
            myPrint("[\(title)]: \(msg) 􀆔\(thread)")
        } else {
            myPrint("[\(title)]: \(msg)")
        }
    }
    
    static func log(title: String, error: Error, thread: Bool = false) {
        onError?(title,error)
        
        guard shouldPass(title: title) else { return }
        if thread {
            myPrint("\n[\(title) ERROR] (\(Thread.current.dbgName)) \(error.detailedDescription_MultiLine)", wrap: true)
        }else {
            myPrint("[\(title) ERROR] \(error.detailedDescription_MultiLine)", wrap: true)
        }
    }
    
    static private func shouldPass(title: String) -> Bool {
        if logFilters.count == 0 {
            return true
        } else {
            return logFilters.contains(title)
        }
    }
    
    static var timestamp : String { return debugDateFormatter.string(from: Date()) }
    
    private static let debugDateFormatter: DateFormatter = { () -> DateFormatter in
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss.SSS"
        return dateFormatter
    }()
}


func myPrint(_ msg: String, wrap: Bool = false) {
    if wrap {
        let txt = "########################################################"
        print(txt)
#if os(macOS)
        logToFileIfNeeded(msg: txt)
#endif
    }
    
#if DEBUG
    print("\(AppCore.timestamp) " + msg)
#else
    os_log("%{public}@", msg)
#endif
    
#if os(macOS)
    logToFileIfNeeded(msg: "\(AppCore.timestamp) \(msg)")
    
    if wrap {
        let txt = "########################################################"
        print(txt)
        logToFileIfNeeded(msg: txt)
    }
#endif
}

#if os(macOS)

@available(macOS 10.0, *)
fileprivate var logFileUrl = FSApp.home.appendingPathComponent("LogFile.txt")

@available(macOS 10.0, *)
fileprivate func logToFileIfNeeded(msg: String) {
    guard AppCore.writeLogToFile else { return }
    
    let file = File(url: logFileUrl )
    
    if !logFileUrl.exists {
        try? file.setContent("")
    }
    
    file.append(msg)
}

#endif
