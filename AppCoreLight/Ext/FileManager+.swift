import Foundation
import AsyncNinja
import Essentials

//based on https://gist.github.com/NikolaiRuhe/408cefb953c4bea15506a3f80a3e5b96

extension FileManager {
    func allocatedSizeOf(file: URL) -> Channel<UInt64, UInt64> {
        producer(executor: .background) { producer in
            do {
                producer.update(1)
                
                let accumulatedSize: UInt64 = try file.regularFileAllocatedSize()
                
                //hack for use in .assign(on: self, to: \.folderSize)
                producer.update(accumulatedSize)
                
                producer.succeed(accumulatedSize)
            } catch {
                producer.fail( WTF(error.detailedDescription) )
            }
        }
    }
    
    
    func allocatedSizeOfDirectory(at directoryURL: URL) -> Channel<UInt64, UInt64> {
        producer(executor: .background) { producer in
            var accumulatedSize: UInt64 = 0
            
            producer.update(accumulatedSize)
            
            var enumeratorError: Error? = nil
            func errorHandler(_: URL, error: Error) -> Bool {
                enumeratorError = error
                return false
            }
            
            // We have to enumerate all directory contents, including subdirectories.
            let enumerator = self.enumerator(at: directoryURL,
                                             includingPropertiesForKeys: Array(allocatedSizeResourceKeys),
                                             options: [],
                                             errorHandler: errorHandler)!
            
            for item in enumerator {
                if let enumeratorError = enumeratorError {
                    producer.fail( WTF(enumeratorError.detailedDescription) )
                }
                
                if let contentItemURL = item as? URL {
                    accumulatedSize += try contentItemURL.regularFileAllocatedSize()
                    
                    producer.update(accumulatedSize)
                }
            }
            
            producer.succeed(accumulatedSize)
        }
    }
}

fileprivate let allocatedSizeResourceKeys: Set<URLResourceKey> = [
    .isRegularFileKey,
    .fileAllocatedSizeKey,
    .totalFileAllocatedSizeKey,
]

fileprivate extension URL {
    func regularFileAllocatedSize() throws -> UInt64 {
        let resourceValues = try self.resourceValues(forKeys: allocatedSizeResourceKeys)
        
        guard resourceValues.isRegularFile ?? false else {
            return 0
        }
        
        // To get the file's size we first try the most comprehensive value in terms of what
        // the file may use on disk. This includes metadata, compression (on file system
        // level) and block size.
        
        // In case totalFileAllocatedSize is unavailable we use the fallback value (excluding
        // meta data and compression) This value should always be available.
        
        return UInt64(resourceValues.totalFileAllocatedSize ?? resourceValues.fileAllocatedSize ?? 0)
    }
}
