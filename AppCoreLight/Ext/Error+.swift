import Foundation

public extension Error {
    var descriptionTitle : String {
        var result = "\((self as NSError).code)"
        if let domain = __domain {
            result += "\(domain)"
        }
        
        result += " : " + self.localizedDescription
        
        return result
    }
    
    var detailedDescription_MultiLine : String {
        var result = ""
        
        result += "ERROR : " + self.localizedDescription
        result += "\nCODE   : \((self as NSError).code)"
        
        if let domain = __domain {
            result += "\(domain)"
        }
        
        if let reason = __reason {
            result += "\n\(reason)"
        }
        
        if let userInfo = __userInfo {
            result += "\n\(userInfo)"
        }
        
        return result
    }
    
    var detailedDescription : String {
        var result = __code + " " + self.localizedDescription
        
        if let reason = __reason {
            result += " | \(reason)"
        }
        
        if let domain = __domain {
            result += " | \(domain)"
        }
        
        if let userInfo = __userInfo {
            result += " | \(userInfo)"
        }
        
        return result
    }
}

extension Error {
    public var __reason : String? {
        let nsError = self as NSError
        if let reason = nsError.localizedFailureReason {
            return "REASON: \(reason)"
        }
        return nil
    }
    
    var __code : String {
        let nsError = self as NSError
        return "[code: \(nsError.code)]"
    }
    
    var __domain : String? {
        let nsError = self as NSError
        
        if nsError.domain != "" {
            return " @ \(nsError.domain)"
        }
        return nil
    }
    
    public var __userInfo : String? {
        let nsError = self as NSError
        
        var userInfo = nsError.userInfo
        userInfo[NSLocalizedDescriptionKey] = nil
        userInfo[NSLocalizedFailureReasonErrorKey] = nil
        if !userInfo.isEmpty {
            return "UserInfo: \(userInfo)"
        }
        
        return nil
    }
}
