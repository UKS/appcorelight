public extension AppCore {
    static func initAll() {
        _ = AppCore.signals
    }
}

public class AppCore {
    public static let signals = SignalsService()
    
    public static var logFilters = [String]()
    
    public static var onError : ((String,Error) -> Void)?
    
    public static var writeLogToFile: Bool = false
}
