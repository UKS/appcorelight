import Foundation
import AsyncNinja
import SwiftUI
import Combine
import Essentials

@available(OSX 10.15, *)
public protocol ConfigPropertyProtocol : ObservableObject {
    associatedtype Element
    
    var didSet          : Channel <Element, Void> { get }
    var didSetFromTo    : Channel<(Element,Element),Void> { get }
    var value           : Element { get set }
}

@available(OSX 10.15, *)
public extension ConfigProperty {
    var asBinding  : Binding<T> { Binding(get: { self.value }, set: { self.value = $0 }) }
    var asPublisher: Optional<T>.Publisher { [self.value].publisher.first() }
}

@available(OSX 10.15, *)
public extension ConfigPropertyEnum {
    var asBinding  : Binding<T> { Binding(get: { self.value }, set: { self.value = $0 }) }
    var asPublisher: Optional<T>.Publisher { [self.value].publisher.first() }
}

//@available(OSX 10.15, *)
//public extension ConfigPropertyRaw where T : OptionSet, T.RawValue == UInt32, T.Element == T {
//    func asBinding(with option: T) -> Binding<T> {
//        return Binding(get: { self.value.contains(option) }, set: { self.value = self.value.with(option, on: $0) })
//    }
//}


@available(OSX 10.15, *)
public class ConfigProperty<T : Equatable> : ConfigPropertyProtocol  {
    public let objectWillChange = PassthroughSubject<ConfigProperty, Never>()
    
    public let store           : ConfigBackend
    public let key             : String
    public let defaultValue    : T
    
    public var didSet           : Channel<T, Void>      { return $valueR }
    public var didSetS          : Flow.Signal<T> = Flow.Signal<T>(queue: .main)
    public var didSetFromTo     : Channel<(T,T),Void>   { return valueFromTo }
    
    @WrappedProducer private var valueR : T
    private let valueFromTo     = Producer<(T,T),Void>()
    
    public var value           : T {
        set { set(value: newValue) }
        get { return valueR }
    }
    
    public init(key: String, defaultValue: T, store: ConfigBackend) {
        self.key = key
        self.defaultValue = defaultValue
        self.store = store
        
        let value = store.value(key: key, ofType: T.self) as? T
        self.valueR = value ?? defaultValue
        
        if let str = value as? String {
            if str == "" {
                setDefault()
            }
        }
    }
    
    public func setDefault() {
        value = defaultValue
    }
    
    private func set(value: T) {
        //AppCore.log(title: "ConfigR", msg: "\(key) - \(value)", thread: true)
        store.set(value: value, key: key)
        valueFromTo.update((valueR,value))
        valueR = value
        didSetS.update(value)
        objectWillChange.send(self)
    }
    
}

@available(OSX 10.15, *)
public class ConfigPropertyRaw<T: RawRepresentable> : ObservableObject where T.RawValue : Equatable  {
    public let objectWillChange = PassthroughSubject<ConfigPropertyRaw<T>, Never>()
    
    public let store           : ConfigBackend
    public let key             : String
    public let defaultValue    : T
    
    public var didSet           : Channel<T, Void>      { return $valueR }
    public var didSetFromTo     : Channel<(T,T),Void>   { return valueFromTo }
    
    @WrappedProducer private var valueR : T
    private let valueFromTo     = Producer<(T,T),Void>()

    public var value           : T {
        set { set(value: newValue) }
        get { return valueR }
    }

    public init(key: String, defaultValue: T, store: ConfigBackend) {
        self.key = key
        self.defaultValue = defaultValue
        self.store = store

        if let storedValue = store.value(key: key, ofType: T.RawValue.self) as? T.RawValue {
            let value = T(rawValue: storedValue)
            self.valueR = value ?? defaultValue
        } else {
            self.valueR = defaultValue
        }
    }

    public func setDefault() {
        value = defaultValue
    }

    private func set(value: T) {
        store.set(value: value.rawValue, key: key)
        valueFromTo.update((valueR,value))
        valueR = value
        objectWillChange.send(self)
    }
}

@available(OSX 10.15, *)
public class ConfigPropertyEnum<T: RawRepresentable> : ConfigPropertyRaw<T> where T.RawValue : Equatable  {}
