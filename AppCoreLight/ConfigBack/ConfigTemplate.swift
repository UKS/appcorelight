/////////////////////////////////////
///Config template
/////////////////////////////////////

/*
 
 
 import Foundation
 import AppCoreLight

 class Config : NinjaContext.Main  {
     static let shared: Config = initSharedConfig()

     var preludePassed : ConfigProperty<Bool>
     
     init(store: ConfigBackend) {
 //        store.clear()
         preludePassed           = store.property(key: "preludePassed", defaultValue: false )
         
     }
 }

 ////////////////////////
 ///HELPERS
 ///////////////////////

 fileprivate func initSharedConfig() -> Config {
     let se: ServiceEnvironment = .Release
     let cud = ConfigUserDefaults(env: se)
     
     return Config(store: cud)
 }

 
 */



/////////////////////////////////////
///Config usage in View
/////////////////////////////////////

/*
 
 
 struct ConfigGeneralView: View {
     @ObservedObject var achievementEnabledCp: ConfigProperty<Bool>
    
     init() {
         achievementEnabledCp = Config.shared.achievementsEnabled
     }
    
     var body: some View {
         Toggle(isOn: achievementEnabledCp.asBinding) { }
     }
 }
 
 
 */
