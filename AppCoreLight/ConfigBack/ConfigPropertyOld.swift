import Foundation
import AsyncNinja

public class ConfigPropertyOld<T : Equatable> : NinjaContext.Main  {
    public let store           : ConfigBackend
    public let key             : String
    public let defaultValue    : T
    
    public var didSet           : Channel<T, Void>      { return $valueR }
    public var didSetFromTo     : Channel<(T,T),Void>   { return valueFromTo }
    
    @WrappedProducer private var valueR : T
    private let valueFromTo     = Producer<(T,T),Void>()
    
    public var value           : T {
        set { set(value: newValue) }
        get { return valueR }
    }
    
    public init(key: String, defaultValue: T, store: ConfigBackend) {
        self.key = key
        self.defaultValue = defaultValue
        self.store = store
        
        let value = store.value(key: key, ofType: T.self) as? T
        self.valueR = value ?? defaultValue
    }
    
    func storedValue() -> T {
        (store.value(key: key, ofType: T.self) as? T) ?? defaultValue
    }
    
    public func setDefault() {
        value = defaultValue
    }
    
    private func set(value: T) {
        store.set(value: value, key: key)
        valueFromTo.update((valueR,value))
        valueR = value
    }
    
}


public class ConfigPropertyRawOld<T: RawRepresentable> : NinjaContext.Main where T.RawValue : Equatable  {
    
    public let store           : ConfigBackend
    public let key             : String
    public let defaultValue    : T
    
    public var didSet           : Channel<T, Void>      { return $valueR }
    public var didSetFromTo     : Channel<(T,T),Void>   { return valueFromTo }
    
    @WrappedProducer private var valueR : T
    private let valueFromTo     = Producer<(T,T),Void>()

    public var value           : T {
        set { set(value: newValue) }
        get { return valueR }
    }

    public init(key: String, defaultValue: T, store: ConfigBackend) {
        self.key = key
        self.defaultValue = defaultValue
        self.store = store

        if let storedValue = store.value(key: key, ofType: T.RawValue.self) as? T.RawValue {
            let value = T(rawValue: storedValue)
            self.valueR = value ?? defaultValue
        } else {
            self.valueR = defaultValue
        }
        
        //valueR = storedValue() ?? defaultValue
    }
    
    func storedValue() -> T {
        if let storedValue = store.value(key: key, ofType: T.RawValue.self) as? T.RawValue {
            return T(rawValue: storedValue) ?? defaultValue
        } else {
            return defaultValue
        }
    }

    public func setDefault() {
        value = defaultValue
    }

    private func set(value: T) {
        store.set(value: value.rawValue, key: key)
        valueFromTo.update((valueR,value))
        valueR = value
    }
}
