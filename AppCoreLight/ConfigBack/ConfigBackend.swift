import Foundation

public protocol ConfigBackend {
    func set(value: Any?,    key: String)
    func value<T: Equatable> (key: String, ofType: T.Type) -> Any?
    func clear()
}

@available(OSX 10.15, *)
public extension ConfigBackend {
    func property<T>(key: String, defaultValue: T) -> ConfigProperty<T> {
        return ConfigProperty(key: key, defaultValue: defaultValue, store: self)
    }
    
    func propertyRawRepresentable<T>(key: String, defaultValue: T) -> ConfigPropertyRaw<T> {
        return ConfigPropertyRaw(key: key, defaultValue: defaultValue, store: self)
    }
    func propertyEnumRepresentable<T>(key: String, defaultValue: T) -> ConfigPropertyEnum<T> {
        return ConfigPropertyEnum(key: key, defaultValue: defaultValue, store: self)
    }
}

public extension ConfigBackend {
    func property<T>(key: String, defaultValue: T) -> ConfigPropertyOld<T> {
        return ConfigPropertyOld(key: key, defaultValue: defaultValue, store: self)
    }
    
    func propertyRawRepresentable<T>(key: String, defaultValue: T) -> ConfigPropertyRawOld<T> {
        return ConfigPropertyRawOld(key: key, defaultValue: defaultValue, store: self)
    }
}
