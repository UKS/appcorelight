import Foundation

public enum ServiceEnvironment : String {
    case Release
    case Debug
    case Test
    
    public var shortName : String {
        switch self {
        case .Release:  return "r"
        case .Debug:    return "d"
        case .Test:     return "t"
        }
    }
    
    public var isTest : Bool {
        return self == .Test
    }
    
    public static var runtime : ServiceEnvironment {
        #if DEBUG
        return .Debug
        #else
        return .Release
        #endif
    }
}

public func isTestsRunning() -> Bool {
    let environment = ProcessInfo().environment
    return (environment["XCInjectBundleInto"] != nil);
}

func isDebug() -> Bool {
    #if DEBUG
        return true
    #else
        return false
    #endif
}

func environment() -> ServiceEnvironment {
    if isTestsRunning() {
        return .Test
    }
    return isDebug() ? .Debug : .Release
}
