Super Light version of "AppCore" repository
==

!!!
This is not package! 
You need add this as git-submodule!
!!!

URLs:
* git@gitlab.com:UKS/appcorelight.git
* https://gitlab.com/UKS/appcorelight.git

AppCore init:
==

```
import SwiftUI
import AppCoreLight

@main
struct MySuperApp: App {
    @NSApplicationDelegateAdaptor(AppDelegate.self) var appDelegate : AppDelegate
    
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
```
```
import AppCoreLight

class AppDelegate: NSObject, NSApplicationDelegate {
    func applicationWillFinishLaunching(_ notification: Notification) {
        AppCore.initAll()
    }
}
```

----
----
----
----
----
----
----
----
----
Config
===
Config template / Config usage sample:

https://gitlab.com/UKS/appcorelight/-/blob/main/AppCoreLight/ConfigBack/ConfigTemplate.swift


----
----
----
----
----
----
----
----
----

